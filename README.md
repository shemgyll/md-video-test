# Video test:

## Markdown (remote video)
`![filesamples.com mp4 sample 640x360](https://filesamples.com/samples/video/mp4/sample_640x360.mp4)`:
![filesamples.com mp4 sample 640x360](https://filesamples.com/samples/video/mp4/sample_640x360.mp4)

## Markdown (local video)
`![filesamples.com mp4 sample 640x360](sample_640x360_local.mp4)`:
![filesamples.com mp4 sample 640x360](sample_640x360_local.mp4)

## Regular iFrame:
<!-- blank line -->
<figure class="video_container">
  <video controls="true" allowfullscreen="true" poster="path/to/poster_image.png">
    <source src="https://filesamples.com/samples/video/mp4/sample_640x360.mp4" type="video/mp4">
  </video>
</figure>
<!-- blank line -->

## Minimal iFrame:
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://filesamples.com/samples/video/mp4/sample_640x360.mp4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

